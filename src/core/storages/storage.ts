import {MMKV} from 'react-native-mmkv';

export const mmkvStorage = new MMKV();

const get = <T = string>(key: string, needDeserialized: boolean = true): T | undefined => {
  try {
    const value = mmkvStorage.getString(key);
    if (!value) {
      return undefined;
    }
    return value && needDeserialized ? JSON.parse(value) : value;
  } catch (e) {
    // saving error
    return undefined;
  }
};

const set = async <T = string | Object>(key: string, value: T): Promise<void> => {
  try {
    await mmkvStorage.set(key, typeof value === 'string' ? value : JSON.stringify(value));
  } catch (e) {
    // getting error
  }
};

const remove = async (key: string): Promise<void> => {
  try {
    await mmkvStorage.delete(key);
  } catch (e) {
    // getting error
  }
};

export const storage = {
  get,
  set,
  remove,
};
