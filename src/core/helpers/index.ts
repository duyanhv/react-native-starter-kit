import merge from 'lodash/merge';

export * from './get-responsive-size';
export * from './sleep';
export * from './get-color';
export * from './lighten-color';

export {merge};
