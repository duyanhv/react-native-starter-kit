/* eslint-disable @typescript-eslint/no-explicit-any */
import React, {useEffect, useCallback, useContext, useState} from 'react';
import {setJSExceptionHandler, setNativeExceptionHandler} from 'react-native-exception-handler';
import Promise from 'bluebird';
import {useLoading} from './loading.context';
import {AppError, recordError} from '../exceptions';
import {isArray} from 'lodash';
// import useLogout from './auth/useLogout';
// import useAuthState from './auth/useAuthState';
import axios from 'axios';
interface ErrorHandlerProviderProps {
  children?: React.ReactNode;
}

const ErrorHandlerContext = React.createContext(undefined);

const ErrorHandlerProvider = (props: ErrorHandlerProviderProps): JSX.Element => {
  const {children} = props;

  // const logout = useLogout();
  // const { authenticated } = useAuthState();
  const {setLoading} = useLoading();
  const handleError = useCallback((err): void => {
    const {name, message, stack} = err;
    // if (err.isAxiosError) {
    //     const httpStatus = err.response.status;
    //     if (httpStatus === 401) {
    //         const errorMessage =
    //             err.response &&
    //             err.response.data.errors &&
    //             isArray(err.response.data.errors)
    //                 ? err.response.data.errors[0].message
    //                 : appErrorMessage().network.internalServerError;
    //         // notify(errorMessage, { type: 'error' });
    //         // if (authenticated) {
    //         //     logout();
    //         // }
    //     } else if (httpStatus === 403) {
    //         // notify(appErrorMessage().network.forbidden, {
    //         //     type: 'error',
    //         // });
    //     } else if (
    //         httpStatus === 408 ||
    //         httpStatus === 500 ||
    //         err.code === networkErrorCode.AXIOS_TIMEOUT
    //     ) {
    //         // notify(appErrorMessage().network.internalServerError, {
    //         //     type: 'error',
    //         // });
    //     }
    // } else {
    //     // notify(appErrorMessage().network.internalServerError, {
    //     //     type: 'error',
    //     // });
    // }
    // only record error if it's not app error
    if (err.name !== 'AppError') {
      recordError(err);
    }
  }, []);
  useEffect(() => {
    // For most use cases:
    // registering the error handler (maybe u can do this in the index.android.js or index.ios.js)
    setJSExceptionHandler((err: Error, _isFatal: boolean) => {
      handleError(err as AppError);
      // This is your custom global error handler
      // You do stuff like show an error dialog
      // or hit google analytics to track crashes
      // or hit a custom api to inform the dev team.
    }, true);

    // For most use cases:
    setNativeExceptionHandler(
      async (_exceptionString) => {
        // await logError({
        //     isNative: true,
        //     message: _exceptionString,
        //     stack: _exceptionString,
        //     createdAt: moment().unix(),
        // });
        // mmkvStore().setItem('native_crash', _exceptionString);
        // This is your custom global error handler
        // You do stuff like show an error dialog
        // or hit google analytics to track crashes
        // or hit a custom api to inform the dev team.
        // handleError(new Error(_exceptionString) as AppError);
      },
      false,
      false,
    );

    // https://stackoverflow.com/questions/48487089/global-unhandledrejection-listener-in-react-native
    // We use the "Bluebird" lib for Promises, because it shows good perf
    // and it implements the "unhandledrejection" event:
    // (global as any).Promise = Promise;

    // Global catch of unhandled Promise rejections:
    // (global as any).onunhandledrejection = (err: AppError): void => {
    //     setLoading(false);
    //     // Warning: when running in "remote debug" mode (JS environment is Chrome browser),
    //     // this handler is called a second time by Bluebird with a custom "dom-event".
    //     // We need to filter this case out:
    //     handleError(err);
    // };
  }, [handleError, setLoading]);

  return <ErrorHandlerContext.Provider value={undefined}>{children}</ErrorHandlerContext.Provider>;
};
export {ErrorHandlerProvider};
interface LogErrorRequest {
  message: string;
  stack: string;
  isNative: boolean;
  createdAt: number;
}
