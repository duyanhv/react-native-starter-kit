import React, {useContext, useEffect, useMemo, useState, useCallback} from 'react';
// import firebaseAuth, {FirebaseAuthTypes} from '@react-native-firebase/auth';
import {usePersistenceImmer} from '@core/hooks/use-persistence';
import {config} from '@core/config';
import {AppError, configError} from '@core/exceptions';
import {useLanguage} from '@core/contexts/language.context';
import {useImmer} from 'use-immer';
import {logEvent, configAnalytics, logAuthEvent} from '@core/analytics';
import {EVENT_NAME} from '@app/app.constants';

interface AuthProviderProps {
  children?: React.ReactNode;
}

export type SignInType = 'EMAIL' | 'PHONE_NO' | 'FACEBOOK' | 'GOOGLE' | 'APPLE';

interface AuthState {
  userId: string;
  displayName?: string;
  avatarUrl?: string;
  signInType: SignInType;
  isSignedIn: boolean;
  initializing: boolean;
  isTester: boolean;
}

interface SignUpEmailParams {
  email: string;
  password: string;
}

interface SignInEmailParams extends SignUpEmailParams {}

interface Dispatch {
  signInEmail: (params: SignInEmailParams) => Promise<boolean>;
  signOut: () => Promise<void>;
  setIsTester: (isTester: boolean) => Promise<void>;
}

const AUTH_KEY = 'AUTH';
const DEFAULT_AUTH: AuthState = {
  userId: '',
  avatarUrl: '',
  displayName: '',
  signInType: 'EMAIL',
  isSignedIn: true,
  initializing: true,
  isTester: false,
};

const AuthContext = React.createContext(DEFAULT_AUTH);
const AuthDispatchContext = React.createContext<Dispatch>(undefined as never);

// let confirmationResult: FirebaseAuthTypes.ConfirmationResult | undefined;

const AuthProvider = (props: AuthProviderProps): JSX.Element => {
  const {children} = props;
  const {language} = useLanguage();
  const [auth, setAuth] = useImmer(DEFAULT_AUTH);
  const [initializing, setInitializing] = useState(true);
  const [setAuthPersistence] = usePersistenceImmer(auth, setAuth, AUTH_KEY);

  const signInEmail = async (params: SignInEmailParams): Promise<boolean> => {
    try {
      const {email, password} = params;
      // Sign the user in with the credential
      //   await firebaseAuth().signInWithEmailAndPassword(email, password);
      logAuthEvent('SIGN_IN', 'EMAIL');
    } catch (err) {
      if (err.code === 'auth/user-disabled') {
        throw new AppError('USER_DISABLED', 'auth:userDisabledError');
      }
      if (err.code === 'auth/wrong-password' || err.code === 'auth/user-not-found') {
        throw new AppError('WRONG_CREDENTIAL', 'auth:wrongCredentialError');
      }
      if (err.code === 'auth/too-many-requests') {
        throw new AppError('TOO_MANY_REQUESTS', 'auth:tooManyRequestsError');
      }
      throw err;
    }
    return true;
  };

  const signOut = async (): Promise<void> => {
    setAuthPersistence((draft) => Object.assign(draft, DEFAULT_AUTH, {isTester: draft.isTester}));
    // if (firebaseAuth().currentUser) {
    //   await firebaseAuth().signOut();
    // }
    try {
      //   await GoogleSignin.revokeAccess();
      //   await GoogleSignin.signOut();
    } catch (err) {
      // ignore err
    }
  };

  const setIsTester = async (isTester: boolean): Promise<void> => {
    // setAuthPersistence((draft) => {
    //   draft.isTester = isTester;
    // });
  };

  const dispatch = useMemo(
    (): Dispatch => ({
      signInEmail,
      signOut,
      setIsTester,
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [auth],
  );
  const authState = useMemo(() => ({...auth, initializing}), [auth, initializing]);
  return (
    <AuthContext.Provider value={authState}>
      <AuthDispatchContext.Provider value={dispatch}>{children}</AuthDispatchContext.Provider>
    </AuthContext.Provider>
  );
};

const useAuth = (): {auth: AuthState; dispatch: Dispatch} => {
  const auth = useContext(AuthContext);
  const dispatch = useContext(AuthDispatchContext);
  return {auth, dispatch};
};

export {AuthProvider, useAuth};
