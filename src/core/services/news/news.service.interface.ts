export interface News {
  categoryId: number;
  companyId: null | number;
  coverImage: string;
  createdAt: number;
  fromDate: null | number;
  id: number;
  image: string;
  interviewee: null | string;
  intervieweeInfo: null | string;
  isActive: 0 | 1;
  isPictureNews: 0 | 1;
  isSticky: 0 | 1;
  isVideoNews: 0 | 1;
  layout: number;
  numOfComments: null | number;
  numOfGallery: number;
  numOfLikes: null | number;
  numOfShares: null | number;
  numOfViews: number;
  portraitImage: null | string;
  publishDate: number;
  shortDescription: string;
  shortDescriptionEn: null | string;
  shortNewsViewId: {
    categoryId: number;
    id: number;
  }; // You might want to specify a more specific type for this
  smallImage: string;
  stickyExpireDate: null | number;
  title: string;
  titleEn: null | string;
  toDate: null | number;
  updatedAt: number;
  url: null | string;
  video: null | string;
}
export interface GetAllParams {
  page: number;
  size: number;
}
