import axiosInstance from '../axios/axios-config';
import {Category, PaginationData} from '../common.service.interface';
import {GetAllParams, News} from './news.service.interface';

const API_PATH = {
  GET_ALL: 'shortNewsView/findRelatedNews',
  GET_CATEGORY_NAME: 'categoryNew/findAllByStatus',
};

const getAll = async ({page, size}: GetAllParams): Promise<PaginationData<News> | undefined> => {
  const request = {
    sort: 'publishDate,desc',
    size: size,
    page: page,
    // versionReview: NetWorkConfig.VERSION_APP
  };
  const result = await axiosInstance.get(API_PATH.GET_ALL, {params: request});
  if (result && result.data) {
    return result.data;
  }
  return undefined;
};

const getAllCategoriesName = async (): Promise<Category[] | undefined> => {
  const result = await axiosInstance.get(API_PATH.GET_CATEGORY_NAME);
  if (result && result.data) {
    return result.data;
  }
  return undefined;
};

export const newsServices = {
  getAll,
  getAllCategoriesName,
};
