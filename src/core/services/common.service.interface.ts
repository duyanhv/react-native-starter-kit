export interface PaginationData<T> {
  content: T[];
  first: boolean;
  last: boolean;
  number: number;
  numberOfElements: number;
  size: number;
  sort: Sort[];
  totalElements: number;
  totalPages: number;
}
export interface Sort {
  ascending: boolean;
  descending: boolean;
  direction: 'ASC' | 'DESC';
  ignoreCase: boolean;
  nullHandling: 'NATIVE' | 'NULL_FIRST' | 'NULL_LAST';
  property: string;
}

export interface Category {
  createdAt: number;
  id: number;
  image: null;
  name: string;
  nameEn: string;
  slugName: null;
  status: number;
  type: number;
  updatedAt: number;
}
