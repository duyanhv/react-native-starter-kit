import {config} from '@app/core/config';
import axios from 'axios';
const TIME_OUT = 60 * 1000;
const axiosInstance = axios.create({
  baseURL: config().api.BASE_URL,
  timeout: TIME_OUT,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
  },
});

export default axiosInstance;
