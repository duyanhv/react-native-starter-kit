import {BottomTabBar as RawBottomTabBar, BottomTabBarProps} from '@react-navigation/bottom-tabs';
import React from 'react';
import {BlurView} from 'expo-blur';
export const BottomTabBar = (props: BottomTabBarProps) => (
    //TODO giam opacity opacity: 0.8 khi scroll len xuong
  <BlurView tint='light' intensity={100} style={{position: 'absolute', bottom: 0, left: 0, right: 0, }}>
    <RawBottomTabBar {...props} />
  </BlurView>
);
