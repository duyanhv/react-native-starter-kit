export * from './button/button.component';
export * from './text/text.component';
export * from './text-input/text-input.component';
export * from './view/view.component';
export * from './layout/layout.component';
export * from './list-item/list-item.component';
export * from './loading/loading.component';
export * from './blur/blur.component';
export * from './picker/picker.component';
export * from './picker-input/picker-input.component';
export * from './internet-connectivity/internet-connectivity.component';
export * from './confirmation/confirmation.component';
export * from './autocomplete-input/autocomplete-input.component';
export * from './autocomplete-multiple-input/autocomplete-multiple-input.component';
export * from './date-picker/date-picker.component';
export * from './time-picker/time-picker.component';
export * from './datetime-picker-input/datetime-picker-input.component';
export * from './form-input/form-input.component';
export * from './icon/icon.component';
export * from './loading-modal/loading-modal.component';
export * from './section-header/section-header.component';
export * from './bottom-tab-bar/bottom-tab-bar.component';
export * from './header-bar/header-bar.component';

// export here so if we want to group and customize later we can do it without go to every file and change
export {StatusBar, Alert, ScrollView, Pressable, Image, FlatList, TouchableOpacity} from 'react-native';
export {TabView, SceneMap, TabBar} from 'react-native-tab-view';
export {
  Provider as PaperProvider,
  DefaultTheme,
  Appbar,
  Card,
  ActivityIndicator,
  Avatar,
  Badge,
  Banner,
  Checkbox,
  List,
  Divider,
  Switch,
  IconButton,
  Menu,
  ProgressBar,
} from 'react-native-paper';
