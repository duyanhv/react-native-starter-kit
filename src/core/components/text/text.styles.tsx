import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  h1: {
    fontSize: 24,
    fontWeight: 'bold',
    fontFamily: 'SF Pro',
  },
  h2: {
    fontSize: 22,
    fontWeight: 'bold',
  },
  h3: {
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'New York',
  },
  h4: {
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'New York',
  },
  h5: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'SF Pro',
  },
  h6: {
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'SF Pro',
  },
  label: {
    fontSize: 15,
    fontWeight: '800',
    fontFamily: 'SF Pro',
  },
  p: {
    fontSize: 15,
    fontFamily: 'SF Pro',
  },
});
