import {BottomTabHeaderProps} from '@react-navigation/bottom-tabs';
import React from 'react';
import {BlurView} from 'expo-blur';
import {Image} from 'expo-image';
import {Icon, TabBar, TouchableOpacity, View} from '@core/components';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {hp, wp} from '@app/core/helpers';
import {useAppTheme} from '@app/core/contexts';
import {NavigationState, SceneRendererProps} from 'react-native-tab-view';
import {Avatar} from 'react-native-paper';
import {DrawerActions, useNavigation} from '@react-navigation/native';
import {MODAL_SCREEN_NAME} from '@app/app.constants';
import {DrawerToggleButton} from '@react-navigation/drawer';
import { PlatformPressable } from '@react-navigation/elements';
export const HeaderBar = (props) => {
  const navigation = useNavigation();
  const insets = useSafeAreaInsets();
  const {appTheme} = useAppTheme();
  return (
    <BlurView
      style={{
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 1,
      }}
      tint='light'>
      <View
        row
        style={{
          backgroundColor: 'transparent',
          paddingTop: insets.top,
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 15,
        }}>
        <PlatformPressable onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
          <Avatar.Icon size={40} icon='account-multiple' />
        </PlatformPressable>
        <Image
          style={{
            width: wp(30),
            height: hp(5),
            resizeMode: 'cover',
          }}
          source={require('../../../../assets/images/logo_2x.png')}
          contentFit='contain'
          transition={1000}
          blurRadius={0}
        />
        <TouchableOpacity onPress={() => navigation.navigate(MODAL_SCREEN_NAME.SETTINGS_MODAL as never)}>
          <Icon name={'cog-outline'} size={25} />
        </TouchableOpacity>
      </View>
      <TabBar
        bounces
        {...props}
        indicatorStyle={{backgroundColor: appTheme.colors.primary}}
        style={{backgroundColor: 'transparent'}}
      />
    </BlurView>
  );
};
