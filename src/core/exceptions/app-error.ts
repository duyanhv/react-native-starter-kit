import { AxiosError, AxiosRequestConfig } from 'axios';

const errorNames = {
    appError: 'AppError',
    serverError: 'ServerError',
};

export class AppError extends Error {
    code: string;

    messageCode?: string;

    messageData?: Object | AxiosError;

    constructor(
        code: string,
        message?: string,
        messageData?: Object | AxiosError
    ) {
        super(code);
        this.code = code;
        this.messageCode = message;
        this.messageData = messageData;
        this.name = errorNames.appError;
    }
}

export class ServerError extends Error {
    config: AxiosRequestConfig;
    code?: string | undefined;
    request?: any;
    response?: import('axios').AxiosResponse<any> | undefined;
    isAxiosError: boolean;
    toJSON!: () => object;
    name!: string;
    message: string;
    stack?: string | undefined;

    constructor({
        config,
        message,
        request,
        response,
        isAxiosError,
        stack,
    }: {
        config: AxiosRequestConfig;
        message: string;
        request: any;
        response: import('axios').AxiosResponse<any>;
        isAxiosError: boolean;
        stack: string;
    }) {
        super(message);
        this.config = config;
        this.message = message;
        this.isAxiosError = isAxiosError;
        this.request = request;
        this.response = response;
        this.stack = stack;
        this.name = errorNames.serverError;
    }
}

