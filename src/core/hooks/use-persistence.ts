import {storage} from '@app/core/storages';
import {useEffect, Dispatch, SetStateAction} from 'react';

export const usePersistence = <T>(
  state: T,
  setState: Dispatch<SetStateAction<T>>,
  key: string,
  needDeserialized: boolean = true,
): [Dispatch<SetStateAction<T>>] => {
  const setStatePersistence: Dispatch<SetStateAction<T>> = (newStateOrSetState) => {
    if (typeof newStateOrSetState === 'function') {
      setState((prevState) => {
        const newState = (newStateOrSetState as (prevState: T) => T)(prevState);
        storage.set(key, newState);
        return newState;
      });
    } else {
      setState(newStateOrSetState);
      storage.set(key, newStateOrSetState);
    }
  };
  useEffect(() => {
    (async () => {
      const storageState = await storage.get<T>(key, needDeserialized);
      if (storageState && storageState !== state) {
        setState(storageState);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return [setStatePersistence];
};

/**
 * Generates a custom hook for persisting state using Immer.
 *
 * @template T - The type of the state.
 * @param {T} state - The current state.
 * @param {(f: (draft: T) => void | T) => void} setState - The function for updating the state.
 * @param {string} key - The key used for storing the state in local storage.
 * @param {boolean} [needDeserialized=true] - Indicates whether the state needs to be deserialized from local storage.
 * @return {[(f: (draft: T) => void | T) => void]} - A function for updating the persisted state.
 */
export const usePersistenceImmer = <T>(
  state: T,
  setState: (f: (draft: T) => void | T) => void,
  key: string,
  needDeserialized: boolean = true,
): [(f: (draft: T) => void | T) => void] => {
  const setStatePersistence = (func: (draft: T) => void | T): void => {
    const overrideFunc = (draft: T): void | T => {
      const result = func(draft);
      if (typeof result !== 'undefined') {
        storage.set(key, result);
      } else {
        storage.set(key, draft);
      }
    };
    setState(overrideFunc);
  };
  useEffect(() => {
    (async () => {
      const storageState = await storage.get<T>(key, needDeserialized);
      if (storageState && storageState !== state) {
        setState(() => storageState);
      }
    })();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return [setStatePersistence];
};
