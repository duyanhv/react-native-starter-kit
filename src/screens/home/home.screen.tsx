import {HeaderBar, Layout, SceneMap, TabView, View, Text} from '@app/core/components';
import {useAppTheme} from '@app/core/contexts';
import React from 'react';
import {StyleSheet, useWindowDimensions} from 'react-native';
import {DrawerLayout, DrawerPosition} from 'react-native-gesture-handler';
import {HomePage} from './components/home-page/home-page.component';
type Props = {};

function Home(props: Props) {
  const {appTheme, dispatch} = useAppTheme();

  const layout = useWindowDimensions();
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Trang chủ'},
    {key: 'second', title: 'Second'},
  ]);
  function SecondView() {
    return <View style={{flex: 1, backgroundColor: '#ff4081'}} />;
  }
  const renderScene = SceneMap({
    first: HomePage,
    second: SecondView,
  });
  return (
    <Layout>
      
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{width: layout.width}}
        overScrollMode={'always'}
        renderTabBar={(props) => <HeaderBar {...props} />}
      />
    </Layout>
  );
}
const styles = StyleSheet.create({});
export const HomeScreen = Home;
