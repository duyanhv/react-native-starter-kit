import {Layout, ScrollView, View} from '@app/core/components';
import {useScrollToTop} from '@react-navigation/native';
import React from 'react';
import Animated, {
  useSharedValue,
  withSequence,
  withSpring,
  ReduceMotion,
  FadeInRight,
  FadeOutLeft,
} from 'react-native-reanimated';
import {TouchableWithoutFeedback} from 'react-native';
import {AppleNewsCard} from '../news-card/apple-news-card.component';
import {useTranslation} from 'react-i18next';
import {LanguageNameSpace} from '@app/i18n';
import {useQueries} from '@tanstack/react-query';
import {newsServices} from '@app/core/services';
import {BouncyTouchable} from '../bouncy-touchable/bouncy-touchable.component';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {hp} from '@app/core/helpers';

type Props = {};

export const HomePage = (props: Props) => {
  const {t} = useTranslation<LanguageNameSpace>('home');

  // Scroll to top when clicking on the bottomtab icon
  const ref = React.useRef(null);
  useScrollToTop(ref);
  const insets = useSafeAreaInsets();

  const results = useQueries({
    queries: [
      {queryKey: ['news', 1], queryFn: () => newsServices.getAll({page: 1, size: 10}), staleTime: Infinity},
      {queryKey: ['categories', 2], queryFn: newsServices.getAllCategoriesName, staleTime: Infinity},
    ],
  });
  const newsData = results[0].data;
  const categoriesData = results[1].data;
  return (
    <ScrollView
      ref={ref}
      scrollEventThrottle={800}
      // onScroll={(event) => {
      //   console.log('====================================');
      //   console.log(event.nativeEvent.contentOffset);
      //   console.log('====================================');
      // }}
      contentContainerStyle={{
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 60,
        paddingTop: insets.top + hp(5) + 30,
      }}>
      {newsData &&
        newsData.content &&
        newsData.content.length > 0 &&
        newsData.content.map((item, index) => {
          return (
            <View style={{marginTop: 25}} key={index}>
              <BouncyTouchable>
                <AppleNewsCard news={item} category={categoriesData} />
                {/* <NewsCard news={item} category={categoriesData} /> */}
              </BouncyTouchable>
            </View>
          );
        })}
    </ScrollView>
  );
};
