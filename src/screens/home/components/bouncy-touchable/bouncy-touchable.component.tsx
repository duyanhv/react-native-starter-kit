import React from 'react';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import Animated, {ReduceMotion, useSharedValue, withSequence, withSpring} from 'react-native-reanimated';

type Props = {
  children: JSX.Element;
};
export const BouncyTouchable = (props: Props) => {
  const scaleValue = useSharedValue(1);
  const handlePress = () => {
    scaleValue.value = withSequence(
      withSpring(0.96, {reduceMotion: ReduceMotion.System}),
      withSpring(1, {reduceMotion: ReduceMotion.System}),
    );
  };
  return (
    <TouchableWithoutFeedback
      onPress={handlePress}
      onPressIn={() => {
        scaleValue.value = withSpring(0.96, {mass: 1});
      }}
      onPressOut={() => {
        scaleValue.value = withSpring(1, {mass: 1});
      }}>
      <Animated.View style={{transform: [{scale: scaleValue}]}}>{props.children}</Animated.View>
    </TouchableWithoutFeedback>
  );
};
