import {Avatar, Button, Icon, Text, View} from '@app/core/components';
import {lightenColor, wp} from '@app/core/helpers';
import React from 'react';
import {Dimensions, TouchableOpacity} from 'react-native';
import {Image} from 'expo-image';
import {config} from '@app/core/config';
import {useTranslation} from 'react-i18next';
import {LanguageNameSpace} from '@app/i18n';
import moment from 'moment';
import {News} from '@app/core/services/news/news.service.interface';
import {COLORS_LOOKUP, useAppTheme} from '@app/core/contexts';
import {Category} from '@app/core/services/common.service.interface';
import {LinearGradient} from 'expo-linear-gradient';
const blurhash =
  '|rF?hV%2WCj[ayj[a|j[az_NaeWBj@ayfRayfQfQM{M|azj[azf6fQfQfQIpWXofj[ayj[j[fQayWCoeoeaya}j[ayfQa{oLj?j[WVj[ayayj[fQoff7azayj[ayj[j[ayofayayayj[fQj[ayayj[ayfjj[j[ayjuayj[';

function ActionButton({iconName, description, onPress}: {iconName: string; description: string; onPress: () => void}) {
  return (
    <TouchableOpacity onPress={onPress} style={{flexDirection: 'row', alignItems: 'center', columnGap: 7, padding: 5}}>
      <Icon name={iconName} color={COLORS_LOOKUP.GRAY.darkColor} size={20} />
      <Text style={{color: COLORS_LOOKUP.GRAY.darkColor}} type='h5'>
        {description}
      </Text>
    </TouchableOpacity>
  );
}
function getCategoryNameById(id: number, data: Category[]): string | undefined {
  if (!data) {
    return '';
  }
  const result = data.find((item) => item.id === id);
  return result ? result.name : '';
}
type Props = {news: News; category: Category[]};
export const AppleNewsCard = (props: Props): JSX.Element => {
  const {appTheme} = useAppTheme();
  const {news} = props;
  const {t} = useTranslation<LanguageNameSpace>('home');
  return (
    <View
      style={{
        // marginTop: 25,
        width: wp(90),
        // paddingTop: wp(5),
        // paddingLeft: wp(5),
        // paddingRight: wp(5),
        // paddingBottom: 10,
        borderRadius: 10,
        backgroundColor: '#7FBDFE',
        borderWidth: 0.2,
        borderColor: '#7FBDFE',
        // shadowColor: '#000',
        // shadowOffset: {
        //   width: 0,
        //   height: 1,
        // },
        // shadowOpacity: 0.22,
        // shadowRadius: 2.22,

        // elevation: 3,
      }}>
      <Image
        style={{
          width: '100%',
          height: 0.25 * Dimensions.get('window').height,
          resizeMode: 'cover',
          //   borderRadius: 10,
          borderTopLeftRadius: 10,
          borderTopRightRadius: 10,
          overflow: 'hidden',
        }}
        source={`${config().api.BASE_URL_IMAGE}${news.smallImage}`}
        placeholder={blurhash}
        contentFit='cover'
        transition={1000}
        blurRadius={0}
      />

      <View>
        <LinearGradient colors={['#fff', '#7FBDFE']} start={{x: 0, y: 0}} end={{x: 1, y: 0}}>
          <Text type='label' style={{color: appTheme.colors.error, padding: 10, marginLeft: 5}}>
            {getCategoryNameById(news.categoryId, props.category)}
          </Text>
        </LinearGradient>
      </View>
      <View style={{padding: 13, marginBottom: 15}}>
        <Text type='h3' style={{color: '#fff', fontFamily: 'New York'}}>
          {news.title}
        </Text>
      </View>
      <View
        style={{padding: 10, paddingHorizontal: 5, borderTopWidth: 0.3, borderTopColor: lightenColor('#7FBDFE', 0.2)}}>
        <Text type='p' style={{color: lightenColor('#7FBDFE', 0.2), fontWeight: '500', marginHorizontal: 11}}>
          {moment(news.publishDate).format(config().dateFormat)}
        </Text>
      </View>
    </View>
  );
};
