import {Avatar, Button, Icon, Text, View} from '@app/core/components';
import {wp} from '@app/core/helpers';
import React from 'react';
import {Dimensions, TouchableOpacity} from 'react-native';
import {Image} from 'expo-image';
import {config} from '@app/core/config';
import {useTranslation} from 'react-i18next';
import {LanguageNameSpace} from '@app/i18n';
import moment from 'moment';
import {News} from '@app/core/services/news/news.service.interface';
import {COLORS_LOOKUP, useAppTheme} from '@app/core/contexts';
import {Category} from '@app/core/services/common.service.interface';
const blurhash =
  '|rF?hV%2WCj[ayj[a|j[az_NaeWBj@ayfRayfQfQM{M|azj[azf6fQfQfQIpWXofj[ayj[j[fQayWCoeoeaya}j[ayfQa{oLj?j[WVj[ayayj[fQoff7azayj[ayj[j[ayofayayayj[fQj[ayayj[ayfjj[j[ayjuayj[';

function ActionButton({iconName, description, onPress}: {iconName: string; description: string; onPress: () => void}) {
  return (
    <TouchableOpacity onPress={onPress} style={{flexDirection: 'row', alignItems: 'center', columnGap: 7, padding: 5}}>
      <Icon name={iconName} color={COLORS_LOOKUP.GRAY.darkColor} size={20} />
      <Text style={{color: COLORS_LOOKUP.GRAY.darkColor}} type='h5'>
        {description}
      </Text>
    </TouchableOpacity>
  );
}
function getCategoryNameById(id: number, data: Category[] | undefined): string | undefined {
  if (!data) {
    return '';
  }
  const result = data.find((item) => item.id === id);
  return result ? result.name : '';
}
type Props = {news: News; category: Category[] | undefined};
export const NewsCard = (props: Props): JSX.Element => {
  const {appTheme} = useAppTheme();
  const {news} = props;
  const {t} = useTranslation<LanguageNameSpace>('home');
  return (
    <View
      style={{
        marginTop: 25,
        rowGap: 15,
        width: wp(90),
        paddingTop: wp(5),
        paddingLeft: wp(5),
        paddingRight: wp(5),
        paddingBottom: 10,
        borderRadius: 10 + wp(5),
        backgroundColor: '#fff',
        borderWidth: 0.2,
        borderColor: 'rgba(0, 0, 0, 0.2)',
        // shadowColor: '#000',
        // shadowOffset: {
        //   width: 0,
        //   height: 1,
        // },
        // shadowOpacity: 0.22,
        // shadowRadius: 2.22,

        // elevation: 3,
      }}>
      <View row style={{justifyContent: 'space-between'}}>
        <View row style={{columnGap: 10, justifyContent: 'center', alignItems: 'center'}}>
          <Avatar.Icon size={40} icon='account-multiple' />
          <View>
            <Text type='label' style={{color: COLORS_LOOKUP.GRAY.darkColor}}>
              {getCategoryNameById(news.categoryId, props.category)}
            </Text>
            <Text type='p' style={{color: COLORS_LOOKUP.GRAY.darkColor}}>
              {moment(news.publishDate).format(config().dateFormat)}
            </Text>
          </View>
        </View>
      </View>

      <View style={{rowGap: 15}}>
        <Text type='h4'>{news.title}</Text>
        <Image
          style={{
            width: '100%',
            height: 0.25 * Dimensions.get('window').height,
            resizeMode: 'cover',
            borderRadius: 10,
            overflow: 'hidden',
            borderWidth: 1,
            borderColor: appTheme.colors.success,
          }}
          source={`${config().api.BASE_URL_IMAGE}${news.smallImage}`}
          placeholder={blurhash}
          contentFit='cover'
          transition={1000}
          blurRadius={0}
        />
        <View row style={{justifyContent: 'flex-start', columnGap: 5}}>
          <ActionButton iconName='heart-outline' description='123' onPress={() => {}} />
          <ActionButton iconName='comment-outline' description='123' onPress={() => {}} />
          <ActionButton iconName='share-outline' description='123' onPress={() => {}} />
        </View>
      </View>
    </View>
  );
};
