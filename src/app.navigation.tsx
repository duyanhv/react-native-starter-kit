/* eslint-disable @typescript-eslint/no-explicit-any */
import React, {useEffect} from 'react';
import {NavigationContainer, LinkingOptions, CommonActions, useNavigation} from '@react-navigation/native';
import {createMaterialBottomTabNavigator} from 'react-native-paper/react-navigation';
import {TransitionSpecs, createStackNavigator, Header} from '@react-navigation/stack';
import {useTranslation} from 'react-i18next';
import {Icon, View, Text, BottomTabBar} from '@core/components';
import {useAppTheme, useAuth} from '@core/contexts';
import {MODAL_SCREEN_NAME, SCREEN_NAME} from '@app/app.constants';
import {trackScreen} from '@core/analytics';
// import {config} from '@core/config';
// import {HomeScreen, SettingsScreen} from '@app/screens';
// import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
// import {PodCastDetailScreen} from '@app/screens/modal';
import {HomeScreen, SettingsScreen, SignInScreen} from './screens';
import {config} from './core/config';
import Animated, {
  cancelAnimation,
  Easing,
  FadeIn,
  FadeOut,
  useAnimatedStyle,
  useSharedValue,
  withDelay,
  withSpring,
  withTiming,
} from 'react-native-reanimated';
import {wp} from './core/helpers';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {Platform, TouchableOpacity, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {BlurView} from 'expo-blur';
import {createDrawerNavigator} from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
// const StackSharedElement = createSharedElementStackNavigator();

interface TabItem {
  name: string;
  title: string;
  icon: string;
  iconFocused: string;
  component: React.FunctionComponent;
}

interface StackItem {
  name: string;
  component: React.FunctionComponent;
}
const MainTabs = (): JSX.Element => {
  const {t} = useTranslation('common');
  const {appTheme} = useAppTheme();
  const navigation = useNavigation();
  const tabItems: TabItem[] = [
    {
      name: SCREEN_NAME.HOME,
      title: t('home'),
      icon: 'home-outline',
      iconFocused: 'home',
      component: HomeScreen,
    },
    // {
    //   name: SCREEN_NAME.BOOKS,
    //   title: t('settings'),
    //   icon: 'book-outline',
    //   iconFocused: 'book',
    //   component: () => <></>,
    // },
    {
      name: SCREEN_NAME.SETTINGS,
      title: t('settings'),
      icon: 'cog-outline',
      iconFocused: 'cog',
      component: SettingsScreen,
    },
  ];
  // const iconAnim = useSharedValue(25);

  return (
    <Tab.Navigator
      initialRouteName={SCREEN_NAME.HOME}
      tabBar={(props) => <BottomTabBar {...props} />}
      // screenListeners={({navigation, route}) => ({
      //   tabPress: (e) => {
      //     iconAnim.value = 20;
      //     iconAnim.value = withTiming(25);
      //   },
      // })}
      screenOptions={({navigation, route}) => ({
        tabBarStyle: [
          {
            // borderTopColor: '#66666666',
            backgroundColor: 'transparent',
            elevation: 0,
          },
        ],
        tabBarShowLabel: false,
      })}>
      {tabItems.map((tabItem, index) => {
        return (
          <Tab.Screen
            key={tabItem.name}
            name={tabItem.name}
            component={tabItem.component}
            options={{
              title: tabItem.title,
              headerShown: false,
              tabBarIcon: (iconProps) => {
                const {focused, color} = iconProps;
                return <Icon name={focused ? tabItem.iconFocused : tabItem.icon} color={color} size={25} />;
              },
              // tabBarButton: (props) => <TouchableOpacity {...props} />,
            }}
          />
        );
      })}
    </Tab.Navigator>
  );
};

export const AppNavigation = (): JSX.Element => {
  const {auth} = useAuth();
  const routeNameRef = React.useRef();
  const navigationRef = React.useRef();
  const {appTheme} = useAppTheme();

  const stackItems: StackItem[] = [
    {
      name: SCREEN_NAME.MAIN_TABS,
      component: MainTabs,
    },
    {name: SCREEN_NAME.SIGN_IN, component: SignInScreen},
  ];

  const linking: LinkingOptions<any> = {
    prefixes: config().deepLink.prefixes,
    config: {
      screens: {
        [SCREEN_NAME.SIGN_IN]: 'signin',
        [SCREEN_NAME.MAIN_TABS]: {
          screens: {
            [SCREEN_NAME.HOME]: 'home',
            [SCREEN_NAME.SETTINGS]: 'settings',
          },
        },
      },
    },
  };
  const TabNavigator = () => (
    <Stack.Navigator initialRouteName={auth.isSignedIn ? SCREEN_NAME.MAIN_TABS : SCREEN_NAME.SIGN_IN}>
      {stackItems.map((stackItem) => (
        <Stack.Screen
          key={stackItem.name}
          name={stackItem.name}
          component={stackItem.component}
          options={{headerShown: false}}
        />
      ))}
      <Stack.Group
        screenOptions={{
          presentation: 'modal',
          headerStyle: {
            backgroundColor: appTheme.colors.background,
            borderBottomWidth: 0,
            shadowRadius: 0,
            shadowOffset: {height: 0, width: 0},
          },
          headerTintColor: appTheme.colors.text,
        }}>
        <Stack.Screen
          name={MODAL_SCREEN_NAME.SETTINGS_MODAL}
          options={{title: 'The Creative Boom Podcast'}}
          component={SettingsScreen}
        />
      </Stack.Group>
    </Stack.Navigator>
  );
  return (
    <NavigationContainer
      ref={navigationRef as any}
      onReady={() => {
        routeNameRef.current = (navigationRef.current as any).getCurrentRoute().name;
      }}
      onStateChange={() => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = (navigationRef.current as any).getCurrentRoute().name;

        if (previousRouteName !== currentRouteName) {
          trackScreen(currentRouteName);
        }

        // Save the current route name for later comparison
        routeNameRef.current = currentRouteName;
      }}
      linking={linking}>
      <Drawer.Navigator screenOptions={{headerShown: false}}>
        <Drawer.Group>
          <Drawer.Screen name='App' component={TabNavigator} />
        </Drawer.Group>
      </Drawer.Navigator>
    </NavigationContainer>
  );
};
