import React, {useEffect, useState} from 'react';
import {I18nextProvider} from 'react-i18next';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {RootSiblingParent} from 'react-native-root-siblings';
import {
  AppThemeProvider,
  useAppTheme,
  LanguageProvider,
  useLanguage,
  InternetConnectionProvider,
  LoadingProvider,
  useLoading,
  ErrorHandlerProvider,
  useAuth,
  AuthProvider,
} from '@core/contexts';
import {merge} from '@core/helpers';
// import {PaperProvider, DefaultTheme, LoadingModal} from '@core/components';
import {usePushNotification} from '@core/hooks';
import {LoadingModal, DefaultTheme, PaperProvider} from '@core/components';
import {i18next} from './i18n';
import {AppNavigation} from './app.navigation';
import {MD3DarkTheme, Provider} from 'react-native-paper';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {Platform, StatusBar, View} from 'react-native';
import {AnimatedBootSplash} from './bootsplash.animated';
import {PersistQueryClientProvider} from '@tanstack/react-query-persist-client';
import {clientPersister, queryClient} from './core/react-query';
import {QueryClientProvider} from '@tanstack/react-query';

export const BaseApp = (): JSX.Element => {
  const [visible, setVisible] = useState(true);
  const {auth} = useAuth();
  const {appTheme} = useAppTheme();
  const {language} = useLanguage();
  const {loading} = useLoading();
  usePushNotification();

  const theme = merge({}, appTheme.theme === 'dark' ? MD3DarkTheme : DefaultTheme, {
    colors: {primary: appTheme.colors.primary},
  });

  useEffect(() => {
    // set transparent status bar
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('transparent');
      StatusBar.setBarStyle('dark-content');
      StatusBar.setTranslucent(true);
    }
  }, []);
  useEffect(() => {
    i18next.changeLanguage(language);
  }, [language]);

  return (
    <>
      <PaperProvider theme={theme}>
        <AppNavigation />
        <LoadingModal loading={loading} />
      </PaperProvider>
      {visible && (
        <AnimatedBootSplash
          onAnimationEnd={() => {
            setVisible(false);
          }}
        />
      )}
    </>
  );
};

export const App = (): JSX.Element => {
  return (
    <RootSiblingParent>
      <QueryClientProvider client={queryClient}>
        <LanguageProvider>
          <I18nextProvider i18n={i18next}>
            <LoadingProvider>
              <AppThemeProvider>
                <SafeAreaProvider>
                  <InternetConnectionProvider>
                    <ErrorHandlerProvider>
                      <AuthProvider>
                        <BaseApp />
                      </AuthProvider>
                    </ErrorHandlerProvider>
                  </InternetConnectionProvider>
                </SafeAreaProvider>
              </AppThemeProvider>
            </LoadingProvider>
          </I18nextProvider>
        </LanguageProvider>
      </QueryClientProvider>
    </RootSiblingParent>
  );
};

// https://reactnative.dev/docs/headless-js-android
export const AppHeadlessCheck = ({isHeadless}: {isHeadless: boolean}): JSX.Element | null => {
  if (isHeadless) {
    // App has been launched in the background by iOS, ignore
    // eslint-disable-next-line no-null/no-null
    return null;
  }

  return <App />;
};
